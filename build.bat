echo Start cleaning and the rebuilding project
MSBuild eVisionTest.sln /target:Rebuild /property:Configuration=Debug
set BUILD_STATUS=%ERRORLEVEL%
if %BUILD_STATUS%==0 echo Build succeeded
if not %BUILD_STATUS%==0 ( 
	echo Build failed
	exit /b
)

echo Start executing tests
MSTest /testcontainer:eVisionTest\bin\debug\eVisionTest.dll

echo Prepare the nuget package
nuget pack eVisionTest\eVisionTest.csproj
