﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eVisionTest
{
    public interface IAccountService
    {
        double GetAccountAmount(int accountId);
    }
}
