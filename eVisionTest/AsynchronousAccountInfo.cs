﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eVisionTest
{
    public class AsynchronousAccountInfo
    {
        private readonly int _accountId;
        private readonly IAsynchronousAccountService _accountService;
        public AsynchronousAccountInfo(int accountId, IAsynchronousAccountService accountService)
        {
            _accountId = accountId;
            _accountService = accountService;
        }
        public double Amount { get; private set; }

        /// <summary>
        /// Notice that the method now can be invoked synchronously multiple times,
        /// yielding execution to the calling thread immediatly, while assigning the 
        /// value to Amount only upon completion.
        /// </summary>
        public async Task RefreshAmount()
        {
            Amount = await _accountService.GetAccountAmount(_accountId);   
        }
    }
}
