﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace eVisionTest
{
    [TestClass]
    public class AccountInfoUnitTests
    {
        [TestMethod]
        public void RefreshAmountTestMethod()
        {
            // Since the method has only one branching path and no inputs
            // it's safe to assume one test covers all the normal non-trivial execution paths,
            // except the case the dependency throws an exception, in which case 
            // RefreshAmount is expected to do the same, but that is a trivial case

            // Arrange
            var testAmount = 1.0;

            var accountServiceMoq = new Mock<IAccountService>(); // dependency mock implementation via Moq framework
            accountServiceMoq.Setup(x => x.GetAccountAmount(It.IsAny<int>())).Returns(testAmount);

            var accountInfo = new AccountInfo(1, accountServiceMoq.Object);

            // Assert
            Assert.AreEqual(accountInfo.Amount, 0.0); // The field is uninitialized, it must be 0

            // Act
            accountInfo.RefreshAmount();

            // Assert
            Assert.AreEqual(accountInfo.Amount, testAmount); // Must have the new value provided by the mocked interface
        }

        [TestMethod]
        public void AsyncRefreshAmountTestMethod()  
        {
            // Arrange
            var testAmount = 1.0;

            // dependency mock implementation via Moq framework
            var accountServiceMoq = new Mock<IAsynchronousAccountService>();
            accountServiceMoq.Setup(x => x.GetAccountAmount(It.IsAny<int>())).Returns(async () =>
            {
                await Task.Delay(500);
                return testAmount;
            });

            var accountInfo = new AsynchronousAccountInfo(1, accountServiceMoq.Object);

            // Act
            var task = Task.Run(async () => // call asynchronously 
            {
                // Call multiple times
                await accountInfo.RefreshAmount(); 
                await accountInfo.RefreshAmount();
                await accountInfo.RefreshAmount();
            });

            // Assert
            Assert.AreEqual(accountInfo.Amount, 0.0); // Should still be 0 immediatly after returning

            task.Wait(); // Notice here the waiting til the work is done in async ...

            Assert.AreEqual(accountInfo.Amount, testAmount); // New the valued should be updated upon completion
        }
    }
}
