# README #

Note I've made 2 versions, first for question 1, second with "Asynchronous" classes
for question 2, and there are 2 tests for each of them of corresponding comments
which should be decently covered. I also use Moq, a standard library, for mocking the
dependencies instead of writing mock implementations myself.

The build script has 3 parts: rebuild, execute tests and package project.
I run the build.bat through Developer Command Prompt, but note that 
nuget.exe is required either through global path or in the folder.